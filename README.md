# Overview

## Estimators' Comparison

### 1. Samples Generation
- Generates random parameter θ using a uniform distribution.
- Creates a series of samples for θ.

### 2. Defining Estimators of θ
- Defines eight estimators (t1 to t8) for θ.
- Stores estimations in a matrix (mat_t).

### 3. Sample Mean, Variance & Quality
- Computes sample means, variances, bias, and MSE for each estimator.
- Discusses which estimators perform better.

### 4. Estimator Samples Boxplot
- Generates boxplots to visualize estimator performance.

### 5. Maximum Likelihood Estimation
- Focuses on MLE for a normally distributed dataset.

#### Log-Likelihood Estimator
- Defines the likelihood function and its derivative.
- Demonstrates how to find the maximum likelihood estimate for σ.

#### Demonstration by R Language
- Provides R code for log-likelihood calculations.
- Creates a table of log-likelihood values and derivatives.

### 6. Histogram and Maximum Likelihood Estimation
- Generates a histogram for a sample dataset.
- Compares it to the estimated normal distribution.

The document analyzes various estimators, their performance, and Maximum Likelihood Estimation with a focus on estimating the standard deviation of a normal distribution. It includes explanations and visualizations.
